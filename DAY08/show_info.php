<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="form_register_8.css">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

</head>

<body>
<form action="show_info.php" method="post" class="showform">
    <table border="0px" width="400" class="table-search">
        <tr height="40px">
            <td class="label name">Khoa</sup></td>
            <td class="space"></td>
            <td class="selectBox">
                <select name="departs" id="depart">
                    <option value="none" selected disabled hidden></option>
                    <option <?php if(isset($_POST['departs']) && $_POST['departs'] == "Khoa học máy tính") echo "selected = 'selected'"; ?> >Khoa học máy tính</option>
                    <option <?php if(isset($_POST['departs']) && $_POST['departs'] == "Khoa học vật liệu") echo "selected = 'selected'"; ?> >Khoa học vật liệu</option>

<!--                    --><?php
//                    $depart = [
//                            "MAT" => "Khoa học máy tính",
//                            "KDL" => "Khoa học vật liệu"
//                    ];
//
//                    foreach ($depart as $key => $value) { ?>
<!--                        <option>--><?//= $value ?><!--</option>-->
<!--                    --><?php //};
                    ?>
                </select>
            </td>
        </tr>
        <tr class="space"></tr>

        <tr height="40px">
            <td class="label name">Từ khóa</td>
            <td class="space"></td>
            <td class="input"><input type="text" name="keyword" id="keyword" value=<?= isset($_POST['keyword']) ? $_POST['keyword'] : ""?>></td>
        </tr>

        <tr height="80px">
            <td colspan="3" align="center">
                <input class="button" type="submit" name="search_info" value="Tìm kiếm">
            </td>
            <td colspan="3" align="center">
                <button type="button" onclick="clear_info()" style="height: 40px;
    width: 100px;
    text-align: center;
    background-color: rgb(89, 185, 103);
    color: rgb(255, 255, 255);
    border-radius: 5px;
    border: 2px solid rgb(0, 133, 177);
    cursor: pointer;">Xóa</button>
            </td>
        </tr>
    </table>
</form>
    <table class="table-result" width="400px">
        <tr height="40px">
            <td colspan="3">
                <div>Số sinh viên tìm thấy:</div>
            </td>
        </tr>
        <tr height="80px">
            <td colspan="3" align="right">
                <button type="submit" onclick="add_info()" style="height: 40px;
    width: 100px;
    text-align: center;
    background-color: rgb(89, 185, 103);
    color: rgb(255, 255, 255);
    border-radius: 5px;
    border: 2px solid rgb(0, 133, 177);
    cursor: pointer;">Thêm</button>
        </tr>
    </table>




<table class="table-show-info" width="800px">
    <tr>
        <th>No</th>
        <th class="table-info" align="left">Ten sin vien</th>
        <th class="table-info" align="left">Khoa</th>
        <th colspan="2">Action</th>
    </tr>
    <tr>
        <td>1</td>
        <td class="table-info">Nguyen Van A</td>
        <td class="table-info">Khoa hoc may tinh</td>
        <td><input class="button" type="submit" name="submit" value="Xóa"></td>
        <td><input class="button" type="submit" name="submit" value="Sửa"></td>
    </tr>
    <tr>
        <td>2</td>
        <td class="table-info">Tran Thi B</td>
        <td class="table-info">Khoa hoc may tinh</td>
        <td><input class="button" type="submit" name="submit" value="Xóa"></td>
        <td><input class="button" type="submit" name="submit" value="Sửa"></td>
    </tr>
    <tr>
        <td>3</td>
        <td class="table-info">Nguyen Hoang C</td>
        <td class="table-info">Khoa hoc vat lieu</td>
        <td><input class="button" type="submit" name="submit" value="Xóa"></td>
        <td><input class="button" type="submit" name="submit" value="Sửa"></td>
    </tr>
    <tr>
        <td>4</td>
        <td class="table-info">Dinh Quang D</td>
        <td class="table-info">Khoa hoc may tinh</td>
        <td><input class="button" type="submit" name="submit" value="Xóa"></td>
        <td><input class="button" type="submit" name="submit" value="Sửa"></td>
    </tr>
</table>

</body>

</html>
<script>
    function add_info(){
        window.location="form_register_7.php";
    }

    function clear_info(){
       var a = $('#depart').val();
       var b = $('#keyword').val();
       // var a = document.getElementById('depart');
       // var b = document.getElementById('keyword');
       a.value = "";
       b.value = "";
    }
</script>