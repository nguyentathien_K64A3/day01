
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Day04</title>
    <link rel="stylesheet" type="text/css" href="form_register_5.css">

    <!-- Bootstrap -->

    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <!-- Bootstrap -->
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />

    <!-- Bootstrap DatePicker -->


</head>



<body>
<?php
session_start();

?>


<table border="0px" width="400">
    <tr height="40px">
        <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
            Họ và tên
        </td>
        <td class="space"></td>
        <td ><?php echo $_SESSION['name']?></td>
    </tr>

    <tr class="space"></tr>

    <tr height="40px">
        <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
            Giới tính
        </td>
        <td class="space"></td>
        <td ><?php echo $_SESSION['gender']?></td>
    </tr>

    <tr class="space"></tr>

    <tr height="40px">
        <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
            Ngày sinh
        </td>
        <td class="space"></td>
        <td ><?php echo $_SESSION['day'].'/'.$_SESSION['month'].'/'.$_SESSION['year']?></td>
    </tr>
    <tr class="space"></tr>

    <tr height="40px">
        <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
            Địa chỉ
        </td>
        <td class="space"></td>
        <td ><?php echo '{'.$_SESSION['district'].'} - {'.$_SESSION['city'].'}'?></td>
    </tr>
    <tr class="space"></tr>

    <tr height="40px">
        <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
            Thông tin khác
        </td>
        <td class="space"></td>
        <td ><?php echo $_SESSION['address']?></td>
    </tr>


</table>

</body>

<script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
<script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function () {
        $('#txtDate').datepicker({
            format: "dd/mm/yyyy"
        });
    });
</script>

</html>

