<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Day05</title>
    <link rel="stylesheet" type="text/css" href="input_student.css">

    <!-- Bootstrap -->

    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <!-- Bootstrap -->
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />

    <!-- Bootstrap DatePicker -->


</head>



<body>
<?php
date_default_timezone_set('Asia/Ho_Chi_Minh');
session_start();

function checkData($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

if (isset($_POST['Register'])){

    if ( empty(checkData($_POST['name'])) || empty($_POST['gender']) || empty($_POST['year']) || empty($_POST['month']) || empty($_POST['district']) || empty($_POST['day']) || empty($_POST['city'])) {
        $error['check'] = '1';
    }


    if(!$error){
        $_SESSION['name'] = $_POST['name'];
        $_SESSION['gender'] = $_POST['gender'];
        $_SESSION['month'] = $_POST['month'];
        $_SESSION['year'] = $_POST['year'];
        $_SESSION['day'] = $_POST['day'];
        $_SESSION['city'] = $_POST['city'];
        $_SESSION['district'] = $_POST['district'];
        $_SESSION['address'] = $_POST['address'];


        header('Location:check.php');
    }
}

?>
<form method="post" enctype="multipart/form-data" style=" height: 100%;
    width: 550px;
    padding: 40px;
    border-style: solid;
    border-color: rgb(0, 133, 177);">
    <h3 style="text-align: center"><u>Form đăng ký sinh viên</u></h3>
    <?php
    if (isset($error)) {
        echo empty(checkData($_POST['name'])) ? '<p style="color: red">Hãy nhập tên.</p></br>' : '' ;
        echo isset($_POST['gender']) ? '': '<p style="color: red">Hãy chọn giới tinh.</p></br>';
        echo isset($_POST['year']) ? '': '<p style="color: red">Hãy chọn năm.</p></br>';
        echo isset($_POST['month']) ? '': '<p style="color: red">Hãy chọn tháng.</p></br>';
        echo isset($_POST['day']) ? '': '<p style="color: red">Hãy chọn ngày.</p></br>';
        echo isset($_POST['city']) ? '': '<p style="color: red">Hãy chọn thành phố.</p></br>';
        echo isset($_POST['district']) ? '': '<p style="color: red">Hãy chọn quận.</p></br>';


    }

    ?>
    <table border="0px" width="650px">
        <tr height="40px">
            <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
                Họ và tên
<!--                <sup style="color: red">*</sup>-->
            </td>
            <td class="space"></td>
            <td class="input"><input type="text" name="name"></td>
        </tr>

        <tr class="space"></tr>

        <tr height="40px">
            <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
                Giới tính
<!--                <sup style="color: red">*</sup>-->
            </td>
            <td class="space"></td>
            <td class="checkBox">
                <label for="gender_nam">
                    <input type="radio" id="gender_nam" name="gender" value="Nam" checked>
                    Nam
                </label>
                <label for="gender_nữ">
                    <input type="radio" id="gender_nữ" name="gender" value="Nữ">
                    Nữ
                </label>

            </td>
        </tr>

        <tr class="space"></tr>

        <tr height="40px">
            <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
                Ngày sinh
<!--                <sup style="color: red">*</sup>-->
            </td>
            <td class="space"></td>
            <?php
            $year = getdate()['year'];
            ?>
            <td class="selectBox">
                Năm
                <select name="year" id="year" >
                    <option value="none" selected disabled hidden></option>
                    <?php
                    for ($i = $year-40; $i <= $year-15; $i++){
                    ?>
                        <option value="<?= $i ?>"><?= $i ?></option>
                    <?php
                        };
                    ?>
                </select>

                Tháng
                <select name="month" id="month" >
                    <option value="none" selected disabled hidden></option>
                    <?php
                    for ($i = 1; $i <= 12; $i++){
                        ?>
                        <option value="<?= $i ?>"><?= $i ?></option>
                        <?php
                    };
                    ?>
                </select>

                Ngày
                <select name="day" id="day" >
                    <option value="none" selected disabled hidden></option>
                    <?php
                    for ($i = 1; $i <= 31; $i++){
                        ?>
                        <option value="<?= $i ?>"><?= $i ?></option>
                        <?php
                    };
                    ?>
                </select>
            </td>

        </tr>
        <tr class="space"></tr>

        <tr height="40px">
            <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
                Địa chỉ
                <!--                <sup style="color: red">*</sup>-->
            </td>
            <td class="space"></td>
            <?php
            $city = [
                'Hà Nội' => [
                    '1' => 'Hoàng Mai',
                    '2' => 'Thanh Trì',
                    '3' => 'Nam Từ Liêm',
                    '4' => 'Hà Đông',
                    '5' => 'Cầu Giấy'
                ],

                'Tp.Hồ Chí Minh' => [
                    '1' => 'Quận 1'
                ]
            ];
            ?>
            <td class="selectBox">
                Thành phố
                <select name="city" id="city" >
                    <option value="none" selected disabled hidden></option>
                    <?php
                    foreach ($city as $key => $value){
                        ?>
                        <option value="<?= $key ?>" onchange="city()"><?= $key ?></option>
                    <?php
                    };
                    ?>
                </select>

                <?php
                $district = [
                    'Hà Nội' => [
                        '1' => 'Hoàng Mai',
                        '2' => 'Thanh Trì',
                        '3' => 'Nam Từ Liêm',
                        '4' => 'Hà Đông',
                        '5' => 'Cầu Giấy'
                    ],

                    'Tp.Hồ Chí Minh' => [
                        '1' => 'Quận 1'
                    ]
                ];
                ?>
                Quận
                <select name="district" id="district" >
                    <option value="none" selected disabled hidden></option>
                    <?php
                    foreach ($district as $key => $value){
                        ?>
                        <option value="<?= $key ?>"><?= $key ?></option>
                        <?php
                    };
                    ?>
                </select>

            </td>

        </tr>
        <tr class="space"></tr>

        <tr height="40px">
            <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
                Thông tin khác
            </td>
            <td class="space"></td>
            <td class="input"><input type="text" name="address"></td>
        </tr>
        <tr class="space"></tr>

        <tr height="80px">
            <td colspan="5" rowspan="5" align="center">
                <button class="button" type="submit" name="Register">Đăng ký</button>
            </td>
        </tr>

        <tr>

    </table>
</form>
</body>

<script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
<script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>

<script type="text/javascript">
    function city(){
        var citis = document.getElementById("city");
        var districts = document.getElementById("district");
        var data1 = [
            'Hoàng Mai', 'Thanh Trì', 'Nam Từ Liêm', 'Hà Đông', 'Cầu Giấy'
        ]
        if (citis == "Hà Nội"){
            for (var i=0; i<data1.length; i++)
            districts.options[districts.options] = new Option(i);
        }
    }
</script>
<script>//<![CDATA[
    if (address_2 = localStorage.getItem('address_2_saved')) {
        $('select[name="calc_shipping_district"] option').each(function() {
            if ($(this).text() == address_2) {
                $(this).attr('selected', '')
            }
        })
        $('input.billing_address_2').attr('value', address_2)
    }
    if (district = localStorage.getItem('district')) {
        $('select[name="calc_shipping_district"]').html(district)
        $('select[name="calc_shipping_district"]').on('change', function() {
            var target = $(this).children('option:selected')
            target.attr('selected', '')
            $('select[name="calc_shipping_district"] option').not(target).removeAttr('selected')
            address_2 = target.text()
            $('input.billing_address_2').attr('value', address_2)
            district = $('select[name="calc_shipping_district"]').html()
            localStorage.setItem('district', district)
            localStorage.setItem('address_2_saved', address_2)
        })
    }
    $('select[name="calc_shipping_provinces"]').each(function() {
        var $this = $(this),
            stc = ''
        c.forEach(function(i, e) {
            e += +1
            stc += '<option value=' + e + '>' + i + '</option>'
            $this.html('<option value="">Tỉnh / Thành phố</option>' + stc)
            if (address_1 = localStorage.getItem('address_1_saved')) {
                $('select[name="calc_shipping_provinces"] option').each(function() {
                    if ($(this).text() == address_1) {
                        $(this).attr('selected', '')
                    }
                })
                $('input.billing_address_1').attr('value', address_1)
            }
            $this.on('change', function(i) {
                i = $this.children('option:selected').index() - 1
                var str = '',
                    r = $this.val()
                if (r != '') {
                    arr[i].forEach(function(el) {
                        str += '<option value="' + el + '">' + el + '</option>'
                        $('select[name="calc_shipping_district"]').html('<option value="">Quận / Huyện</option>' + str)
                    })
                    var address_1 = $this.children('option:selected').text()
                    var district = $('select[name="calc_shipping_district"]').html()
                    localStorage.setItem('address_1_saved', address_1)
                    localStorage.setItem('district', district)
                    $('select[name="calc_shipping_district"]').on('change', function() {
                        var target = $(this).children('option:selected')
                        target.attr('selected', '')
                        $('select[name="calc_shipping_district"] option').not(target).removeAttr('selected')
                        var address_2 = target.text()
                        $('input.billing_address_2').attr('value', address_2)
                        district = $('select[name="calc_shipping_district"]').html()
                        localStorage.setItem('district', district)
                        localStorage.setItem('address_2_saved', address_2)
                    })
                } else {
                    $('select[name="calc_shipping_district"]').html('<option value="">Quận / Huyện</option>')
                    district = $('select[name="calc_shipping_district"]').html()
                    localStorage.setItem('district', district)
                    localStorage.removeItem('address_1_saved', address_1)
                }
            })
        })
    })
    //]]></script>

</html>
