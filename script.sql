CREATE DATABASE QLSV;

USE QLSV:

CREATE TABLE DMKHOA (
  MaKH varchar(6),
  TenKhoa varchar(30),
  CONSTRAINT PK_MaKH PRIMARY KEY (MaKH)
);


CREATE TABLE SINHVIEN (
  MaSV varchar(6) ,
  HoSV varchar(30),
  TenSV varchar(15),
  GioiTinh char(1),
  NgaySinh datetime,
  NoiSinh varchar(50),
  DiaChi varchar(50),
  MaKH varchar(6),
  HocBong int,
  CONSTRAINT PK_MaSV PRIMARY KEY (MaSV),
  CONSTRAINT FK_MaKH FOREIGN KEY (MaKH) REFERENCES DMKHOA (MaKH)
);