<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Day05</title>
    <link rel="stylesheet" type="text/css" href="form_register_5.css">

    <!-- Bootstrap -->

    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <!-- Bootstrap -->
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />

    <!-- Bootstrap DatePicker -->


</head>



<body>
<?php
date_default_timezone_set('Asia/Ho_Chi_Minh');
session_start();

function checkData($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

if (isset($_POST['Register'])){

    $dir = "image";

    if (! file_exists($dir)) {
        mkdir($dir);
    }

    if ( empty(checkData($_POST['name'])) || empty($_POST['gender']) || empty($_POST['depart']) || empty(checkData($_POST['birthday'])) || empty(checkData($_POST['address'])) || empty(checkData($_FILES['image']['name']))) {
        $error['check'] = '1';
    }

    if (! empty(checkData($_FILES['image']['name']))) {
        $files =  $_FILES['image']['tmp_name'];
        $name = $_FILES['image']['name'].date('YmdHis');
        $path = "image/".$name;
        move_uploaded_file($files, $path);
    
        if (! getimagesize("image/".$name)) {
            $error['image'] = '';
        }
    }

    if(!$error){
        $_SESSION['name'] = $_POST['name'];
        $_SESSION['gender'] = $_POST['gender'];
        $_SESSION['depart'] = $_POST['depart'];
        $_SESSION['birthday'] = $_POST['birthday'];
        $_SESSION['address'] = $_POST['address'];
        $_SESSION['image'] = $name;
        
        header('Location:print.php');
    }
}

?>
<form method="post" enctype="multipart/form-data" style=" height: 100%;
    width: 470px;
    padding: 40px;
    border-style: solid;
    border-color: rgb(0, 133, 177);">
    <?php
    if (isset($error)) {
        echo empty(checkData($_POST['name'])) ? '<p style="color: red">Hãy nhập tên.</p></br>' : '' ;
        echo isset($_POST['gender']) ? '': '<p style="color: red">Hãy chọn giới tinh.</p></br>';
        echo isset($_POST['depart']) ? '': '<p style="color: red">Hãy chọn phân khoa.</p></br>';
        echo empty(checkData($_POST['birthday'])) ? '<p style="color: red">Hãy nhập ngày sinh.</p></br>' : '' ;
        echo empty(checkData($_POST['address'])) ? '<p style="color: red">Hãy nhập địa chỉ.</p></br>' : '' ;
        echo empty(checkData($_FILES['image']['name'])) ? '<p style="color: red">Hãy nhập ảnh.</p></br>' : '' ;
        echo empty($error['image']) ? '<p style="color: red">Hãy nhập ảnh.</p></br>' : '' ;
    }

    ?>
    <table border="0px" width="600px">
        <tr height="40px">
            <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
                Họ và tên <sup style="color: red">*</sup>
            </td>
            <td class="space"></td>
            <td class="input"><input type="text" name="name"></td>
        </tr>

        <tr class="space"></tr>

        <tr height="40px">
            <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
                Giới tính <sup style="color: red">*</sup>
            </td>
            <td class="space"></td>
            <td class="checkBox">
                <?php
                $Gender =[
                    0 => "Nam",
                    1 => "Nữ"
                ];

                for ($i = 0; $i < 2; $i++) { ?>
                    <label for="<?= $Gender[$i] ?>">
                        <input type="radio" id="<?= $Gender[$i] ?>" name="gender" value="<?= $Gender[$i]?>">
                        <?= $Gender[$i]?>
                    </label>
                    <?php
                };
                ?>
            </td>
        </tr>

        <tr class="space"></tr>

        <tr height="40px">
            <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
                Phân Khoa <sup style="color: red">*</sup>
            </td>
            <td class="space"></td>
            <td class="selectBox">
                <select name="depart" id="depart" >
                    <option value="none" selected disabled hidden></option>
                    <?php
                    $depart = [
                        "MAT" => "Khoa học máy tính",
                        "KDL" => "Khoa học vật liệu"
                    ];
                    foreach ($depart as $key => $value) {
                        ?>
                        <option value="<?= $key ?>"><?= $value ?></option>
                        <?php
                    };
                    ?>
                </select>
            </td>
        </tr>
        <tr class="space"></tr>

        <tr height="40px">
            <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
                Ngày sinh <sup style="color: red">*</sup>
            </td>
            <td class="space"></td>
            <td class="input"><input id="txtDate" type="text"  name="birthday" placeholder="dd/mm/yyyy" readonly="readonly"></td>
        </tr>
        <tr class="space"></tr>

        <tr height="40px">
            <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
                Địa chỉ
            </td>
            <td class="space"></td>
            <td class="input"><input type="text" name="address"></td>
        </tr>
        <tr class="space"></tr>

        <tr height="40px">
        <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
            Hình ảnh
        </td>
        <td class="space"></td>
        <td><input type="file" name="image"></td>
        </tr>

        <tr height="80px">
            <td colspan="5" rowspan="5" align="center">
                <button class="button" type="submit" name="Register">Đăng ký</button>
            </td>
        </tr>
    </table>
</form>
</body>

<script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
<script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function () {
        $('#txtDate').datepicker({
            format: "dd/mm/yyyy"
        });
    });
</script>

</html>
