<div>
    <select class="form-select form-select-sm mb-3" id="city" aria-label=".form-select-sm">
        <option value="" selected>Chọn tỉnh thành</option>
    </select>

    <select class="form-select form-select-sm mb-3" id="district" aria-label=".form-select-sm">
        <option value="" selected>Chọn quận huyện</option>
    </select>

</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>
<script>
    var citis = document.getElementById("city");
    var districts = document.getElementById("district");
    var wards = document.getElementById("ward");
    var Parameter = {
        url: "data.json",
        method: "GET",
        responseType: "application/json",
    };
    var promise = axios(Parameter);
    promise.then(function (result) {
        renderCity(result.data);
    });

    function renderCity(data) {
        for (const x of data) {
            citis.options[citis.options.length] = new Option(x.Name, x.Id);
        }
        citis.onchange = function () {
            district.length = 1;
            ward.length = 1;
            if(this.value != ""){
                const result = data.filter(n => n.Id === this.value);

                for (const k of result[0].Districts) {
                    district.options[district.options.length] = new Option(k.Name, k.Id);
                }
            }
        };
        // district.onchange = function () {
        //     ward.length = 1;
        //     const dataCity = data.filter((n) => n.Id === citis.value);
        //     if (this.value != "") {
        //         const dataWards = dataCity[0].Districts.filter(n => n.Id === this.value)[0].Wards;
        //
        //         for (const w of dataWards) {
        //             wards.options[wards.options.length] = new Option(w.Name, w.Id);
        //         }
        //     }
        // };
    }
</script>





<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Day04</title>
    <link rel="stylesheet" type="text/css" href="form_register_5.css">

    <!-- Bootstrap -->

    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <!-- Bootstrap -->
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />

    <!-- Bootstrap DatePicker -->


</head>



<body>
<?php
session_start();

?>


    <table border="0px" width="400">
        <tr height="40px">
            <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
                Họ và tên
            </td>
            <td class="space"></td>
            <td ><?php echo $_SESSION['name']?></td>
        </tr>

        <tr class="space"></tr>

        <tr height="40px">
            <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
                Giới tính
            </td>
            <td class="space"></td>
            <td ><?php echo $_SESSION['gender']?></td>
        </tr>

        <tr class="space"></tr>

        <tr height="40px">
            <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
                Phân Khoa
            </td>
            <td class="space"></td>
            <td ><?php echo $_SESSION['depart']?></td>
        </tr>
        <tr class="space"></tr>

        <tr height="40px">
            <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
                Ngày sinh
            </td>
            <td class="space"></td>
            <td ><?php echo $_SESSION['birthday']?></td>
        </tr>
        <tr class="space"></tr>

        <tr height="40px">
            <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
                Địa chỉ
            </td>
            <td class="space"></td>
            <td ><?php echo $_SESSION['address']?></td>
        </tr>
        <tr class="space"></tr>

        <tr height="40px">
            <td style=" padding: 5px; height: 40px; width: 47%; background-color: rgb(120, 158, 207); color: white; text-align: center; border: 2px solid rgb(0, 133, 177);">
                Hình ảnh
            </td>
            <td class="space"></td>

            <td ><img src="image/<?= $_SESSION['image']?>"></td>
        </tr>
        <tr class="space"></tr>

        <tr height="80px">
            <td colspan="5" rowspan="5" align="center">
                <button class="button" >Xác nhận</button>
            </td>
        </tr>
    </table>

</body>

<script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
<script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function () {
        $('#txtDate').datepicker({
            format: "dd/mm/yyyy"
        });
    });
</script>

</html>

