<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="form.css">
</head>

<body>
<form action="" method="post">
    <table border="0px" width="800">
        <?php
        $ques = [
            0 => [
                "question" => "a,b,c,d",
                "choice" => [
                    1 => "a", 
                    2 => "b", 
                    3 => "c", 
                    4 => "d"
                ],
                "answer" => "a"
            ],
            1 => [
                "question" => "a,b,c,d",
                "choice" => [
                    1 => "a", 
                    2 => "b", 
                    3 => "c", 
                    4 => "d"
                ],
                "answer" => "a"
            ],
            2 => [
                "question" => "a,b,c,d",
                "choice" => [
                    1 => "a", 
                    2 => "b", 
                    3 => "c", 
                    4 => "d"
                ],
                "answer" => "a"
            ],
            3 => [
                "question" => "a,b,c,d",
                "choice" => [
                    1 => "a", 
                    2 => "b", 
                    3 => "c", 
                    4 => "d"
                ],
                "answer" => "a"
            ],
            4 => [
                "question" => "a,b,c,d",
                "choice" => [
                    1 => "a", 
                    2 => "b", 
                    3 => "c", 
                    4 => "d"
                ],
                "answer" => "a"
            ]
        ];
        $size = count($ques);
        if (!empty($_POST['submit'])) {
            $_SESSION["mark"] = 0;
            for($i = 0; $i < $size; $i++){
                if(!empty($_POST[$i])){
                    if($_POST[$i] == $ques[$i]['answer']){
                        $_SESSION["mark"] += 1;
                    }
                }

            }
            header("Location: page2.php");
        }

        for ($i = 0; $i < $size; $i++) { ?>
            <tr height="40px">
                <td><?= $ques[$i]["question"] ?></td>
            </tr>
            <?php
            for ($j = 1; $j <= 4; $j++) { ?>
                <tr>
                    <td>
                        <input type="radio" id=<?=$j ?> name=<?=$i?> value=<?=$ques[$i]["choice"][$j]?>>
                        <label for=<?=$j ?>><?= $ques[$i]["choice"][$j] ?></label>
                    </td>
                </tr>
            <?php };
            ?>
        <?php };
        ?>
        <tr height="40px">
            <td align="center"><input class="button" type="submit" name="submit" value="Next"></td>
        </tr>
    </table>
</form>
</body>

</html>

