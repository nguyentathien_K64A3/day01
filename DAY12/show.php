<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="form.css">
</head>

<body>
<form action="" method="post" class="showform">
    <table border="0px" width="400" class="table-search">
        <?php
        if (array_key_exists('button2', $_POST)) {
            button1();
        } else if (array_key_exists('button2', $_POST)) {
            button2();
        }
        function button1()
        {
            echo "This is Button1 that is selected";
        }
        function button2()
        {
            header("Location: register.php");
        }
        $_SESSION = $_POST;
        ?>
        <tr height="40px">
            <td class="label name">Khoa</sup></td>
            <td class="space"></td>
            <td class="selectBox">
                <select name="departs" id="depart">
                    <option value = 'none' selected='selected' disabled hidden></option>
                    <option <?php if(isset($_SESSION['departs']) && $_SESSION['departs'] == "Khoa học máy tính") echo "selected = 'selected'"; ?> >Khoa học máy tính</option>
                    <option <?php if(isset($_SESSION['departs']) && $_SESSION['departs'] == "Khoa học vật liệu") echo "selected = 'selected'"; ?> >Khoa học vật liệu</option>
                </select>
            </td>
        </tr>
        <tr class="space"></tr>
        <tr height="40px">
            <td class="label name">Từ khóa</td>
            <td class="space"></td>
            <td class="input"><input type="text" name="name" id="keyword" value=<?=isset($_SESSION['name']) ? $_SESSION['name'] : ""?>></td>
        </tr>
        <tr height="80px">
            <td align="right"><input class="button" type="button" id="button3" value="Xóa"></td>
            <td></td>
            <td colspan="3" align="center"><input class="button" type="submit" id="button1" value="Tìm kiếm"></td>
        </tr>

    </table>
    <table class="table-result" width="400px">
        <tr height="40px">
            <td colspan="3">
                <div>Số sinh viên tìm thấy:</div>
            </td>
        </tr>
        <tr height="80px">
            <td colspan="3" align="right"><input class="button" type="submit" name="button2" value="Thêm"></td>
        </tr>
    </table>
</form>



<table class="table-show-info" width="800px">
    <tr>
        <th>No</th>
        <th class="table-info" align="left">Ten sin vien</th>
        <th class="table-info" align="left">Khoa</th>
        <th colspan="2">Action</th>
    </tr>
    <tr>
        <td>1</td>
        <td class="table-info">Nguyen Van A</td>
        <td class="table-info">Khoa hoc may tinh</td>
        <td><input class="button" type="submit" name="submit" value="Xóa"></td>
        <td><input class="button" type="submit" name="submit" value="Sửa"></td>
    </tr>
    <tr>
        <td>2</td>
        <td class="table-info">Tran Thi B</td>
        <td class="table-info">Khoa hoc may tinh</td>
        <td><input class="button" type="submit" name="submit" value="Xóa"></td>
        <td><input class="button" type="submit" name="submit" value="Sửa"></td>
    </tr>
    <tr>
        <td>3</td>
        <td class="table-info">Nguyen Hoang C</td>
        <td class="table-info">Khoa hoc vat lieu</td>
        <td><input class="button" type="submit" name="submit" value="Xóa"></td>
        <td><input class="button" type="submit" name="submit" value="Sửa"></td>
    </tr>
    <tr>
        <td>4</td>
        <td class="table-info">Dinh Quang D</td>
        <td class="table-info">Khoa hoc may tinh</td>
        <td><input class="button" type="submit" name="submit" value="Xóa"></td>
        <td><input class="button" type="submit" name="submit" value="Sửa"></td>
    </tr>
</table>

</body>
<script src="main.js"></script>

</html>
<?php
