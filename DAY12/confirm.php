<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Day04</title>
    <link rel="stylesheet" type="text/css" href="form.css">
</head>

<body>
<?php
if ($_SESSION['departs'] == "Khoa học máy tính"){
    $_SESSION['codeDeparts'] = "MAT";
}else{
    $_SESSION['codeDeparts'] = "KDL";
}
?>

<form action="conection.php" method="post" enctype="multipart/form-data">
    <table border="0px" width="400">
        <tr height="40px">
            <td class="label">Họ và tên </td>
            <td class="space"></td>
            <td class=""><?= $_SESSION['name'] ?></td>
        </tr>
        <tr class="space"></tr>
        <tr height="40px">
            <td class="label name">Giới tính</td>
            <td class="space"></td>
            <td class=""><?= $_SESSION['gender'] ?></td>
        </tr>
        <tr class="space"></tr>
        <tr height="40px">
            <td class="label name">Phân Khoa</td>
            <td class="space"></td>
            <td class=""><?= $_SESSION['departs'] ?></td>
        </tr>
        <tr class="space"></tr>
        <tr height="40px">
            <td class="label name">Ngày sinh</td>
            <td class="space"></td>
            <td class=""><?= $_SESSION['birthday'] ?></td>
        </tr>
        <tr class="space"></tr>
        <tr height="40px">
            <td class="label name">Địa chỉ</td>
            <td class="space"></td>
            <td class=""><?= $_SESSION['address'] ?></td>
        </tr>
        <tr class="space"></tr>
        <tr height="40px">
            <td class="label name">Hình ảnh</td>
            <td class="space"></td>
            <td class="imge">
                    <span>
                    <img src="upload/<?= $_SESSION['image']?>">
                    </span>
            </td>
        </tr>
        <tr height="80px">
            <td colspan="5" rowspan="5" align="center"><input class="button" type="submit" name="submit" value="Xác nhận"></td>
        </tr>
    </table>
</form>
</body>

</html>

