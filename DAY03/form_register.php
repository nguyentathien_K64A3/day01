<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Day03</title>
    <link rel="stylesheet" href="form_register.css">
</head>

<body>
    <form >
        <table border="0px" width="400">
            <tr height="40px">
                <td class="label name">Họ và tên</td>
                <td class="space"></td>
                <td class="input"><input type="text" name="name"></td>
            </tr>

            <tr class="space"></tr>

            <tr height="40px">
                <td class="label name">Giới tính</td>
                <td class="space"></td>
                <td class="checkBox">
                    <?php
                        $Gender =[
                            0 => "Nam", 
                            1 => "Nữ"
                        ];

                        for ($i = 0; $i < 2; $i++) { ?>
                            <label for="<?= $Gender[$i] ?>">
                                <input type="radio" id="<?= $Gender[$i] ?>" name="gender" value="<?= $Gender[$i]?>">
                                <?= $Gender[$i]?>
                            </label>
                    <?php 
                        };
                    ?>
                </td>
            </tr>

            <tr class="space"></tr>

            <tr height="40px">
                <td class="label name">Phân Khoa</td>
                <td class="space"></td>
                <td class="selectBox">
                    <select name="depart" id="depart" >
                        <option value="none" selected disabled hidden></option>
                        <?php
                            $depart = [
                                "none" => "",
                                "MAT" => "Khoa học máy tính", 
                                "KDL" => "Khoa học vật liệu"
                            ];
                            foreach ($depart as $key => $value) { 
                        ?>
                                <option value="<?= $key ?>"><?= $value ?></option>
                        <?php 
                            };
                        ?>
                    </select>
                </td>
            </tr>

            <tr height="80px">
                <td colspan="5" rowspan="5" align="center">
                    <button class="button" type="submit">Đăng ký</button>
                </td>
            </tr>
        </table>
    </form>
</body>

</html>
